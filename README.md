# Example Project with CMake and Git Submodule

Basic example how to set up a C++/CUDA project using TNL from a Git submodule
and CMake as the build system

The Git submodule is located in [libs/tnl](./libs/tnl/). When cloning the
repository, remember to initialize the submodules:

    git clone --recurse-submodules https://gitlab.com/tnl-project/example-projects/cmake-submodule.git

Alternatively, initialize the submodules after cloning:

    git submodule update --init --recursive

See the [Git Book](https://git-scm.com/book/en/v2/Git-Tools-Submodules) for
details on using Git submodules.

See the [TNL documentation](https://mmg-gitlab.fjfi.cvut.cz/doc/tnl/) for
details on using the library.

## CMake usage

To build the project using CMake, you must first generate a build system.
For example, using the [Ninja generator](https://ninja-build.org/):

    cmake -S . -B build -G Ninja

Then build the project in the `build` directory:

    cmake --build build

See [cmake(1)](https://cmake.org/cmake/help/latest/manual/cmake.1.html) for
details.
